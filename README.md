# Installation
When using this library from outside the repo install it first via `pip3 install --user git+https://gitlab.gwdg.de/lehrenfeld/goe-ngs-templates` or after cloning the repo install it via `python3 setup.py install --user` or `pip3 install . --user`.

## Global settings
In `global_settings.py` some default settings (such as output level, heap size or compile behavior for coefficeint functions) are set. 
To adjust these settings, don't edit `global_settings.py` but simply overwrite them by adding a python file `local_settings.py` to your `PYTHONPATH`. At the end of `global_settings.py` the `local_settings.py` will be loaded. 

# Using / developing
If you are working in the library directly (working on a module, geometry, test or example) we use the hack to add the local goengs files to the python path `import sys`, `sys.path.append('../goengs')`.

# Available (and planned) modules:
  * levelset (WIP): 
    This module is work in progress. Don't use it, yet. 
    Here, several algorithms related to the levelset description of (typically) moving domain problems are gathered. It has several submodules:
    * transport (only preliminary state)...
    * redistancing (not implemented yet)... 
    * mean curvature computation (not implemented yet)...
    * normal extensions: First version in `goengs.module.levelset.normalextension`
    * bulk extensions (not implemented yet)...
  * utils: some utilities that may simplify working with `NGSolve`. Here functionality is also gathered that may be used by different submodules, which functionality however is not really down to that submodule.
    * `mprint` (master print) for output in distributed memory environment
    * `IterativeSolve(r)` for a unified interface for CG/MinRes/GMRes solver
  * Navier Stokes (WIP): Contains an time discretized H(div)-conforming HDG solver:
    * `NavierStokes`(bad name): IMEX based H(div)-conforming HDG solver    
    * `NavierStokesExplicit`: Explicit Euler based H(div)-conforming DG solver

# Examples
In `examples` some simple examples are given that make use of the modules. 
Try them out, e.g. by calling `python/netgen cylinder.py`.