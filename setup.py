from setuptools import find_packages, setup

setup(name="goengs",
      version="0.0.1",
      description="example solver modules for NGSolve/ngsxfem (version of model templates for Goettingen group)",
      packages=find_packages(),
      zip_safe=False,
      classifiers=("Programming Language :: Python :: 3",
                   "Operating System :: OS Independent",
                   "Development Status :: 2 - Pre-Alpha",
                   "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)")
      )
