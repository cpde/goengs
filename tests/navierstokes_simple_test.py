import pytest
import sys

# hack for not-installed development version
sys.path.append('../goengs')
sys.path.append('./')

import ngsolve
from modules.global_settings import *
from modules.NavierStokes import *

import netgen
from netgen import gui, geom2d, meshing
from ngsolve import Mesh, Draw, x,y,z, sin, TaskManager, Redraw, Trace, CoefficientFunction, Integrate, sqrt, Norm, div, Grad, OuterProduct, Cross

from netgen.geom2d import SplineGeometry
from netgen.csg import OrthoBrick, Pnt, CSGeometry

from ngsolve.internal import visoptions
import math

@pytest.mark.parametrize("dimension", [2,3])

def test_NavierStokes(dimension,
                      order=3,
                      divpenalty=1e7,
                      nu=1,
                      omega_rot=(0,0,0)
                      ):
    
    mesh_size = 0.6

    if dimension == 2:
        geo = SplineGeometry()
        geo.AddRectangle( (0, 0), (1, 1), bc = "outer")
        mesh = Mesh( geo.GenerateMesh(maxh=mesh_size))
        X = [x,y]
        u=CoefficientFunction((-x**2,2*x*y))
        p=x
    else:
        cube = OrthoBrick( Pnt(0,0,0), Pnt(1,1,1)).bc("outer")
        geo = CSGeometry()
        geo.Add (cube)
        mesh = Mesh(geo.GenerateMesh(maxh=mesh_size))
        X = [x,y,z]
        u=CoefficientFunction((-x**2,x*y,x*z))
        p=x

    # exact solution and correspondingly manufactured r.h.s.:
    p = p - Integrate(p,mesh)/Integrate(1,mesh)

    gradu = CoefficientFunction(tuple([u[i].Diff(w) for i in range(mesh.dim) for w in X ]),dims=(mesh.dim,mesh.dim))
    epsu = 0.5*(gradu + gradu.trans)
    divu = Trace(gradu)

    if mesh.dim == 2:
        fcori = 2 * omega_rot[2] * CoefficientFunction((-u[1],u[0]))
    else:
        fcori = 2 * Cross(omega_rot,u)

    fvisc = -2*nu*CoefficientFunction(tuple([sum([epsu[i,j].Diff(X[j]) for j in range(mesh.dim)]) for i in range(mesh.dim)]))
    fconv = gradu * u
    fpres = -CoefficientFunction(tuple([CoefficientFunction(p).Diff(w) for w in X]))

    f = fvisc + fconv + fpres + fcori

    timestep = 0.5
    navstokes = NavierStokes (mesh,
                              nu=nu, omega_rot=omega_rot,
                              order=order, timestep = timestep,
                              divpenalty=divpenalty,
                              inflow="", outflow="", wall=".*",
                              ubnd=u
                              )



    navstokes.AddForce(f)
    navstokes.AddDivRHS(divu)
    navstokes.SolveInitial()

    Draw (p, mesh, "pressureexact")
    Draw (navstokes.pressure, mesh, "pressure")
    Draw (navstokes.velocity, mesh, "velocity")
    visoptions.scalfunction='velocity:0'

    t, tend = 0, 5
    with TaskManager(pajetrace=100*1000*1000):
        while t < tend:
            print ("time=",t)
            navstokes.DoTimeStep()
            t = t+timestep
            Redraw()

            l2error = sqrt(Integrate(Norm(navstokes.velocity - u)**2,mesh))
            print("L2 error:", l2error,end="\t")
            divl2error = sqrt(Integrate((div(navstokes.velocity) - divu)**2,mesh))
            print("div-L2 error:", divl2error,end="\t")
            h1seminerror = sqrt(Integrate(Norm(Grad(navstokes.velocity) - gradu)**2,mesh))
            print("H1 semi-error:", h1seminerror,end="\t")
            l2perror = sqrt(Integrate((navstokes.pressure - p)**2,mesh))
            print("L2 pressure error:", l2perror)

        if divpenalty != None and divpenalty > 0:
            assert(l2error < 1e-5)
            assert(divl2error < 1e-5)
            assert(h1seminerror < 1e-5)
            assert(l2perror < 1e-5)
        else:
            assert(l2error < 1e-10)
            assert(divl2error < 1e-10)
            assert(h1seminerror < 1e-10)
            assert(l2perror < 1e-10)


if __name__=="__main__":
    test_NavierStokes(dimension=2)
