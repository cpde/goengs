from ngsolve import *
import sys
sys.path.append('../goengs')
sys.path.append('./')
from modules.global_settings import *
from modules.NavierStokes import *

from ngsolve.internal import visoptions
from geometries.cylinder import GenerateMesh

ngsglobals.msg_level = 8
SetHeapSize(100*1000*1000)
mesh = GenerateMesh(maxh=0.07, curve_order=3)

uinflow = CoefficientFunction( (1.5*4*y*(0.41-y)/(0.41*0.41), 0) )
ubnd_dict = {"inflow" : uinflow }
ubnd = CoefficientFunction([ ubnd_dict[bc] if bc in ubnd_dict.keys() else CoefficientFunction((0,0)) for bc in mesh.GetBoundaries()])


explicit = False
if explicit: 
    timestep = 0.00025
    navstokes = NavierStokesExplicit (mesh, nu=0.001, order=3, timestep = timestep,
                              inflow="inflow", outflow="outlet", wall="wall|cyl",
                              ubnd=ubnd)
    navstokes.SolveInitial(uinflow)
else:
    timestep = 0.001
    navstokes = NavierStokes (mesh, nu=0.001, order=3, timestep = timestep,
                              inflow="inflow", outflow="outlet", wall="wall|cyl",
                              ubnd=ubnd, formulation="HdivHDG")
    navstokes.SolveInitial()

Draw (navstokes.pressure, mesh, "pressure")
Draw (navstokes.velocity, mesh, "velocity")
visoptions.scalfunction='velocity:0'
tend = 0.5
t = 0
with TaskManager(pajetrace=100*1000*1000):
    while t < tend:
        print (t)
        navstokes.DoTimeStep()
        # input("")
        t = t+timestep
        Redraw(blocking=False)
