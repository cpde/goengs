### from https://github.com/NGSolve/modeltemplates
from ngsolve import *
import math

import sys
sys.path.append('../goengs')
sys.path.append('./')

from modules.global_settings import *
from modules.NavierStokes import *

from ngsolve.internal import visoptions

from geometries.naca import GenerateNACA0015Mesh
mesh = GenerateNACA0015Mesh(maxh=0.2,curve_order=3)
Draw (mesh)

# angle of atack
alpha = 5 * math.pi/180
timestep = 0.001

ubnd_dict = {"inflow" : CoefficientFunction( (math.cos(alpha),math.sin(alpha)) ) }
ubnd = CoefficientFunction([ ubnd_dict[bc] if bc in ubnd_dict.keys() else CoefficientFunction((0,0)) for bc in mesh.GetBoundaries()])

navstokes = NavierStokes (mesh, nu=0.0005, order=3, timestep = timestep, 
                              inflow="inflow", outflow="outlet", wall="wall",
                              ubnd=ubnd)
                              
navstokes.SolveInitial()
Draw (navstokes.pressure, mesh, "pressure")
Draw (navstokes.velocity, mesh, "velocity")
visoptions.scalfunction='velocity:0'

tend = 10
t = 0

with TaskManager(pajetrace=100*1000*1000):
    while t < tend:
        print (t)
        navstokes.DoTimeStep()
        t = t+timestep
        Redraw(blocking=True)



